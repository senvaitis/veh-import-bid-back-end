'use strict';
const passport = require('passport');

const vehicleController = require('../controllers').VehicleController;

module.exports = (server) => {

    // Except from GET, all the other methods listed above can have a payload (i.e. the data into the request body).
    server.get('/api/vehicles', (req, res) => {
        vehicleController.getAllVehicles(req, res);
    });

    server.get('/api/vehicles/:id', (req, res) => {
        vehicleController.getVehicle(req, res);
    });

    server.post('/api/vehicles', passport.authenticate('jwt', {session: false}), (req, res) => {
        vehicleController.createVehicle(req, res);
    });

    // PUT or PATCH (sometimes POST) - Update a resource using the data sent,
    server.put('/api/vehicles/:id', (req, res) => {
        vehicleController.updateVehicle(req, res);
    });

    server.delete('/api/vehicles/:id', (req, res) => {
        vehicleController.deleteVehicle(req, res);
    });

    // bidding
    server.post('/api/vehicles/:id/bid', passport.authenticate('jwt', {session: false}), (req, res) => {
        vehicleController.bidForVehicle(req, res);
    });

    server.ws('/api/vehicles/:id/bid', (ws, req) => {
        vehicleController.streamBids(ws, req);
    })
};