'use strict';

const UserController = require('./user-controller');
const VehicleController = require('./vehicle-controller');

module.exports = {
    UserController,
    VehicleController,
};