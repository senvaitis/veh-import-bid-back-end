var ObjectID = require('mongodb').ObjectID;

module.exports = [
    {
        _id: new ObjectID("5d53f41e6254358fe86590ad"),
        username: "seedtester1",
        email: "seedtester1@gmail.com",
        password: "$2b$10$H4ii36fXd9C6nanmH92/j.VRUM3ROhepKP2kGqww7iOMhS/medBAq",
        vehicles: []
    },
    {
        _id: new ObjectID("5d53f41e6254358fe86590ae"),
        username: "seedtester2",
        email: "seedtester2@gmail.com",
        password: "$2b$10$/Imlwhn7sn2RUy8ubyflA.SI1.mQ5DD0uSKPgUn6G/r/8qJBoFYmS",
        vehicles: []
    }
];