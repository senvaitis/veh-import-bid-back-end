const mochaSteps = require('mocha-steps');
const step = mochaSteps.step;


const mocha = require('mocha');
const describe = mocha.describe;
const before = mocha.before;
const it = mocha.it;

const mongoSeeding = require('mongo-seeding');
const Seeder = mongoSeeding.Seeder;
const path = require('path');

const config = {
    database: 'mongodb+srv://senvaitis:dedicated@cluster0-8aqms.mongodb.net/mongo-seeding-test?retryWrites=true&w=majority',
    dropCollections: true
};

before(function () {
    return new Promise((resolve, reject) => {
        console.log('STARTED SEED IMPORT');
        const seeder = new Seeder(config);
        const collections = seeder.readCollectionsFromPath(
            path.resolve('./test/data')
        );

        seeder
        .import(collections)
        .then(() => {
            console.log('SUCCESSFULLY IMPORTED SEED');
            resolve("SUCCESSFULLY IMPORTED SEED")
        })
        .catch(err => {
            console.log('Error importing seed', err);
            reject("Error importing seed");
        });
    });


});