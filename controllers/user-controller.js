'use strict';

const User = require("../models").User;
const {sign} = require('jsonwebtoken');
const bcrypt = require('bcrypt');


const authenticateUser = (username, password) => {
    return new Promise((resolve, reject) => {
        if (!username || !password) {
            return reject({
                errorCode: "MISSING_USERNAME_OR_PASSWORD",
                field: ["password", "username"],
                originalValue: password,
                message: "Missing username or password",
                helpUrl: "no-url"
            });
        }

        User.findOne({username: username}).then((user) => {
                if (!user) {
                    return reject({
                        errorCode: "USER_NOT_FOUND",
                        field: "username",
                        originalValue: username,
                        message: "User not found",
                        helpUrl: "no-url"
                    });
                }

                if (user._doc.username.toLowerCase() !== username.toLowerCase())
                    return null;

                bcrypt.compare(password, user.password, function (err, res) {
                    if (err) return cb(err);
                    if (res === false) {
                        reject({
                            errorCode: "WRONG_PASSWORD",
                            field: "password",
                            originalValue: password,
                            message: "Wrong password",
                            helpUrl: "no-url"
                        });
                    } else {
                        // token valid for 24 hours (expiresIn uses seconds)
                        const token = sign({username: user._doc.username}, process.env.JWT_TOKEN_SECRET, {expiresIn: 60 * 60 * 24});
                        delete user._doc.password;
                        user._doc.token = token;
                        resolve(user)
                    }
                });

            },
            (error) => {
                // internal server error
            });
    })


};

module.exports = {
    loginUser(req, res) {
        const username = req.body.username;
        const password = req.body.password;
        let test = authenticateUser(username, password).then().then((userWithToken) => {
            return res.json(userWithToken);
        }).catch((error) => {
            return res.status(400).json(error);
        });
    },

    registerUser(req, res) {
        User.findOne({username: req.body.username})
            .then(function (user) {

                if (req.body.password !== req.body.passwordConf) {
                    return res.status(400).json({
                        errorCode: "PASSWORDS_DO_NOT_MATCH",
                        field: "passwordConf",
                        originalValue: req.body.passwordConf,
                        message: "Passwords do not match",
                        helpUrl: "no-url"
                    })
                }
                if (user) {
                    return res.status(400).json({
                        errorCode: "USERNAME_TAKEN",
                        field: "username",
                        originalValue: req.body.username,
                        message: "Username is already taken",
                        helpUrl: "no-url"
                    })
                } else {
                    bcrypt.genSalt(10, function (err, salt) {
                        if (err) return next(err);
                        bcrypt.hash(req.body.password, salt, function (err, hash) {
                            if (err) return next(err);
                            User.create(
                                {
                                    username: req.body.username,
                                    email: req.body.email,
                                    password: hash
                                },
                                (err, newUser) => {
                                    if (err) {
                                        console.log(err);
                                        return res.status(500).json({
                                            errorCode: "INTERNAL_SERVER_ERROR",
                                            message: "0x0001",
                                            helpUrl: "no-url"
                                        })
                                    } else {
                                        authenticateUser(newUser.username, req.body.password).then((userWithToken) => {
                                            return res.json(userWithToken);
                                        }).catch((reason) => {
                                            res.status(500).json({
                                                errorCode: "INTERNAL_SERVER_ERROR",
                                                message: "0x0002",
                                                helpUrl: "no-url"
                                            })
                                        });
                                    }
                                }
                            )
                        });
                    });
                }
            })
    },


    getAllUsers(req, res) {
        User.find()
            .then(function (doc) {
                res.send(doc)
            }).catch((err) => {
            res.status(400).send(err);
        });
    },

    getUser(req, res) {
        let id = req.params.id;
        User.findById(id).then(function (doc) {
            res.send(doc)
        }).catch((err) => {
            res.status(400).send(err);
        });
    },

    // deprecated
    createUser(req, res) {
        // confirm that user typed same password twice
        if (req.body.password !== req.body.passwordConf) {
            var err = new Error('Passwords do not match.');
            err.status = 400;
            res.send("passwords do not match");
            return next(err);
        }

        if (req.body.email &&
            req.body.username &&
            req.body.password &&
            req.body.passwordConf) {
            var userData = {
                email: req.body.email,
                username: req.body.username,
                password: req.body.password,
            };
            //use Model.create to insert data into the db
            User.create(userData, function (err, user) {
                if (err) {
                    // return next(err)
                    res.status(400).send(err);
                } else {
                    // return res.redirect('/profile');
                    res.send(user);
                }
            });
        }
    },


    logoutUser(req, res) {
        return res.json({status: "ok"});
    },


    getUserProfile(req, res) {
        User.findById(req.session.userId)
            .exec(function (error, user) {
                if (error) {
                    return next(error);
                } else {
                    // Behind the scenes it converts a valid JavaScript object into a string, then sets the appropriate headers to tell your browser that you are serving JSON, and sends the data back.
                    return res.json({name: user.user, email: user.email});
                }
            });
    },


    updateUser(req, res) {
        User.findById(req.params.id, function (err, doc) {
            if (err) {
                return reject({"error": "failed to retrieve user"});
            }
            doc.username = req.body.username;
            // doc.model = req.body.model;
            // doc.vehicles = req.body.vehicles.toArray();
            doc.save();
        }).then((message) => {
            res.send(message);
        }).catch((err) => {
            res.status(400).send(err);
        })
    },


    // addUserVehicles(req, res) {
    //     let user;
    //     User.findById(req.params.id, function (err, doc) {
    //         if (err) {
    //             return reject({"error": "failed to retrieve user"});
    //         }
    //         // doc.username = req.body.username;
    //         // doc.model = req.body.model;
    //         // doc.vehicles = req.body.vehicles.toArray();
    //         // doc.save();
    //         doc.vehicles.push(req.body.vehicleId);
    //         doc.save();
    //     }).then((message) => {
    //         res.send(message);
    //     }).catch((err) => {
    //         res.status(400).send(err);
    //     });
    // },


    // findUsersByVehicleId(req, res) {
    //     User.find({vehicles: req.params.vehicleId})
    //     .exec(function (err, users) {
    //         if (err) {
    //             if (err.kind === 'ObjectId') {
    //                 return res.status(404).send({
    //                     message: "User not found with given vehicle Id " + req.params.vehicleId
    //                 });
    //             }
    //             return res.status(500).send({
    //                 message: "Error retrieving User with given vehicle Id " + req.params.vehicleId
    //             });
    //         }
    //         res.send(users);
    //     })
    // }
};