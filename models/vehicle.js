'use strict';

const mongoose = require('mongoose');


var vehicleSchema = new mongoose.Schema({
    make: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    year: {
        type: String
    },
    countryA: {
        type: String,
        required: true
    },
    cityA: {
        type: String,
        required: true
    },
    countryB: {
        type: String,
        required: true
    },
    cityB: {
        type: String,
        required: true
    },
    bodyStyle: {
        type: String
    },
    bids: [{
        amount: Number,
        time: {type: Date}
    }],
    currentBid: {
        type: Number
    },
    agreedTermsAndConditions: {
        type: Boolean,
        required: true
    }
}, {collection: 'vehicles'});

vehicleSchema.set('timestamps', true); // this will add createdAt and updatedAt timestamps


var Vehicle = mongoose.model('Vehicle', vehicleSchema);

module.exports = Vehicle;
