'use strict';

var Locator = require("../utilities").Locator;

var Vehicle = require("../models").Vehicle;
const ObjectID = require('mongodb').ObjectID;
var fs = require('fs');
const papa = require('papaparse');
var path = require('path');


module.exports = {
    getAllVehicles(req, res) {
        Vehicle.find()
        .then(function (doc) {
            res.send({vehicles: doc})
        }).catch((err) => {
            res.status(400).send(err);
        });
    },


    getVehicle(req, res) {
        let id = req.params.id;
        Vehicle.findById(id).then(function (doc) {

            Locator.getVehicleCoordinates(doc)
            .then(coordinates => {
                doc._doc.latA = coordinates.coordinatesA.lat;
                doc._doc.lngA = coordinates.coordinatesA.lng;
                doc._doc.latB = coordinates.coordinatesB.lat;
                doc._doc.lngB = coordinates.coordinatesB.lng;
                res.send(doc);
            }).catch((err) => {
                res.status(400).send(err);
            });




        }).catch((err) => {
            res.status(400).send(err);
        });
    },


    createVehicle(req, res) {
        const item = {
            make: req.body.make,
            model: req.body.model,
            year: req.body.year,
            countryA: req.body.countryA,
            cityA: req.body.cityA,
            countryB: req.body.countryB,
            cityB: req.body.cityB,
            bodyStyle: req.body.bodyStyle,
            currentBid: '',
            agreedTermsAndConditions: req.body.agreedTermsAndConditions
        };



        let data = new Vehicle(item);

        Locator.getVehicleCoordinates(data)
        .then(_coordinates => {
            // ignore coordinates
            data.save()
            .then((message) => {
                res.send(message);
            }).catch((err) => {
                console.log(err);
                return res.status(400).json({
                    errorCode: "ERROR_CREATING_VEHICLE",
                    field: "-",
                    message: "Error creating vehicle",
                    helpUrl: "no-url"
                })
            })
        }).catch((err) => {
            res.status(400).send(err);
        });


    },


    updateVehicle(req, res) {
        Vehicle.findById(req.params.id, function (err, doc) {
            if (err) {
                return reject({"error": "failed to retrieve car"});
            }
            doc.make = req.query.make;
            doc.model = req.query.model;
            doc.save();
        }).then((message) => {
            res.send(message);
        }).catch((err) => {
            res.status(400).send(err);
        })
    },


    bidForVehicle(req, res) {
        Vehicle.findById(
            req.params.id,
            (err, vehicle) => {
                if (err) {
                    return res.status(400).send(err);
                }
                // check if bid is valid
                let amounts = vehicle.bids.map((o) => {
                    return o.amount
                });
                let minAmount = vehicle.bids.length ? Math.min(...amounts) : null;
                console.log("minAmount: " + minAmount);

                if ((req.body.amount < minAmount || minAmount === null) && req.body.amount > 0) {
                    Vehicle.findByIdAndUpdate(
                        req.params.id,
                        {
                            $push: {
                                bids: {
                                    amount: req.body.amount,
                                    time: new Date()
                                }
                            },
                            currentBid: req.body.amount
                        },
                        {new: true},
                        (err, doc) => {
                            if (err) {
                                res.status(400).send(err);
                            } else {
                                res.send(doc);
                            }
                        }
                    );
                } else {
                    return res.status(400).json({
                        errorCode: "WRONG_BID",
                        field: "amount",
                        originalValue: req.body.amount,
                        message: "Bid amount should be lower than the last bid and higher than 0",
                        helpUrl: "no-url"
                    });
                }

            }
        );

    },

    streamBids(ws, req) {
        let vehId = req.params.id;
        console.log("Streaming for vehicle: " + vehId);

        var filter = [{
            $match: {
                $and: [
                    {"documentKey._id": {$eq: new ObjectID(vehId)}},
                    {operationType: "update"}]
            }
        }];

        let vehicleWatch = Vehicle.watch(filter).on('change', data => {
            ws.send('{"currentBid": ' + data.updateDescription.updatedFields.currentBid + "}");
            console.log("sent: " + '{"currentBid": ' + data.updateDescription.updatedFields.currentBid + "}");
        });

        ws.on('message', msg => {
            console.log(msg);
            Vehicle.findById(vehId).then(function (doc) {
                ws.send('{"currentBid": ' + doc.currentBid + "}")
            }).catch((err) => {
                console.log(err);
                // res.status(400).send(err);
            });
        });

        ws.on('close', () => {
            console.log('WebSocket was closed');
            vehicleWatch.close();
        })
    },


    deleteVehicle(req, res) {
        let id = req.params.id;
        Vehicle.findByIdAndRemove(id).exec()
        .then((message) => {
            res.send(message);
        }).catch((err) => {
            res.status(400).send(err);
        })
    }
};