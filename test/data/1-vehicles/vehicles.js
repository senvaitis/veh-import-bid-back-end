const ObjectID = require('mongodb').ObjectID;

module.exports = [
    {
        _id: new ObjectID("5d53eaa3f2a2bf85c8ba18cf"),
        make: "Audi",
        model: "TT",
        year: "2015",
        countryA: "United States",
        cityA: "Seattle",
        countryB: "Lithuania",
        cityB: "Vilnius",
        bids: [
            {
                _id: new ObjectID("5d57ff7d12382f4b104a069d"),
                amount: 800,
                time: "2019-08-17T13:22:05.162Z"
            }
        ],
        currentBid: 800
    },
    {
        _id: new ObjectID("5d58006b793642ce679d01f7"),
        make: "Ferrari",
        model: "F430",
        year: "2007",
        countryA: "United States",
        cityA: "Seattle",
        countryB: "Lithuania",
        cityB: "Vilnius",
        bids: [],
        currentBid: ''
    },
    {
        _id: new ObjectID("5d57f31b338eda7fa558b2fa"),
        make: "BMW",
        model: "335i",
        year: "2009",
        countryA: "United States",
        cityA: "Seattle",
        countryB: "Lithuania",
        cityB: "Vilnius",
        bids: [],
        currentBid: ''
    }
];