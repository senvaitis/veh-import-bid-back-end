'use strict';
const passport = require('passport');

const userController = require('../controllers').UserController;


var User = require("../models").User;


module.exports = (server) => {

    server.get('/api/users', (req, res) => {
        userController.getAllUsers(req, res);
    });

    server.get('/api/profile', passport.authenticate('jwt', {session: false}), (req, res) => {
        res.json(req.user);
    });

    server.post('/api/users/login', (req, res) => {
        userController.loginUser(req, res);
    });


    server.route('/api/users/register')
        .post((req, res) => {
                userController.registerUser(req, res);
            }
        );

    server.get('/api/users/:id', (req, res) => {
        userController.getUser(req, res);
    });

    server.post('/api/users/logout', passport.authenticate('jwt', {session: false}), (req, res) => {
        userController.logoutUser(req, res);
    });

    // Route parameters
    server.put('/api/users/:id', (req, res) => {
        userController.updateUser(req, res);
    });

    server.post('/api/users/:id/vehicles', (req, res) => {
        userController.addUserVehicles(req, res);
    });

    server.get('/api/users/vehicle/:vehicleId', (req, res) => {
        userController.findUsersByVehicleId(req, res)
    });

};