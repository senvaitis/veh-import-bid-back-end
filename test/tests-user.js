// import {describe, before, it} from "mocha";
// import {step} from "mocha-steps";
const mochaSteps = require('mocha-steps');
const step = mochaSteps.step;


const mocha = require('mocha');
const describe = mocha.describe;
const before = mocha.before;
const it = mocha.it;
const chai = require('chai');
const assert = chai.assert;
const server = require('../server');

/** require the chai-http plugin **/
var chaiHttp = require('chai-http');

/** use the chai-http plugin **/
chai.use(chaiHttp);


const JWS_REGEX = /^[a-zA-Z0-9\-_]+?\.[a-zA-Z0-9\-_]+?\.([a-zA-Z0-9\-_]+)?$/;

describe('user API test', function () {
    let token;

    step('register when passwords are not equal', function (done) {
        chai.request(server)
            .post('/api/users/register')
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({username: 'autotester2', email: 'autotester2@gmail.com', password: 'qqqqq', passwordConf: 'ttttt'})
            .end(function (err, res) {
                assert.equal(res.status, 400, 'response status should be 400');
                assert.equal(res.type, 'application/json');
                assert.deepEqual(res.body,
                    {
                        errorCode: "PASSWORDS_DO_NOT_MATCH",
                        field: "passwordConf",
                        originalValue: "ttttt",
                        message: "Passwords do not match",
                        helpUrl: "no-url"
                    });
                done();
            });
    });

    step('register', function (done) {
        chai.request(server)
            .post('/api/users/register')
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({
                username: 'autotester2',
                email: 'autotester2@gmail.com',
                password: 'testpass',
                passwordConf: 'testpass'
            })
            .end(function (err, res) {
                assert.equal(res.status, 200, 'response status should be 200');
                assert.equal(res.type, 'application/json');
                assert.match(res.body.token, JWS_REGEX);
                token = res.body.token;
                done();
            });
    });

    step('register when user already exists', function (done) {
        chai.request(server)
            .post('/api/users/register')
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({
                username: 'autotester2',
                email: 'autotester2@gmail.com',
                password: 'testpass',
                passwordConf: 'testpass'
            })
            .end(function (err, res) {
                assert.equal(res.status, 400, 'response status should be 400');
                assert.equal(res.type, 'application/json');
                assert.deepEqual(res.body,
                    {
                        errorCode: "USERNAME_TAKEN",
                        field: "username",
                        originalValue: "autotester2",
                        message: "Username is already taken",
                        helpUrl: "no-url"
                    });
                done();
            });
    });


    step('login with wrong password', function (done) {
        chai.request(server)
            .post('/api/users/login')
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({username: 'autotester2', password: 'wrongpassword'})
            .end(function (err, res) {
                assert.equal(res.status, 400, 'response status should be 200');
                assert.equal(res.type, 'application/json');
                assert.deepEqual(res.body,
                    {
                        errorCode: "WRONG_PASSWORD",
                        field: "password",
                        originalValue: "wrongpassword",
                        message: "Wrong password",
                        helpUrl: "no-url"
                    });
                done();
            });
    });


    step('login with wrong username', function (done) {
        chai.request(server)
            .post('/api/users/login')
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({username: 'nosuchuser', password: 'wrongpassword'})
            .end(function (err, res) {
                assert.equal(res.status, 400, 'response status should be 200');
                assert.equal(res.type, 'application/json');
                assert.deepEqual(res.body,
                    {
                        errorCode: "USER_NOT_FOUND",
                        field: "username",
                        originalValue: "nosuchuser",
                        message: "User not found",
                        helpUrl: "no-url"
                    });
                done();
            });
    });


    step('invalid JWT token', function (done) {
        chai.request(server)
            .post('/api/vehicles')
            .set('content-type', 'application/x-www-form-urlencoded')
            .set('Authorization', "JWT invalid-token")
            .send({
                make: "Peugeot",
                model: "406 Coupe",
                year: "2002",
                location: "Lithuania",
                bodyStyle: "coupe"
            })
            .end(function (err, res) {
                assert.equal(res.status, 401, 'unauthorized');
                done();
            });
    });


    step('get user profile', function (done) {
        chai.request(server)
            .get('/api/profile')
            .set('content-type', 'application/x-www-form-urlencoded')
            .set('Authorization', "JWT " + token)
            .end(function (err, res) {
                assert.equal(res.status, 200);
                assert.equal(res.type, 'application/json');
                // Asserts that object has all and only all of the keys provided
                assert.hasAllKeys(res.body, ["_id", "username", "email", "vehicles", "__v"]);
                assert.equal(res.body.username, 'autotester2');
                assert.equal(res.body.email, 'autotester2@gmail.com');
                done();
            });
    });


    step('logout', function (done) {
        chai.request(server)
            .post('/api/users/logout')
            .set('content-type', 'application/x-www-form-urlencoded')
            .set('Authorization', "JWT " + token)
            .end(function (err, res) {
                assert.equal(res.status, 200);
                assert.equal(res.type, 'application/json');
                // Asserts that object has all and only all of the keys provided
                assert.deepEqual(res.body, {status: "ok"});
                done();
            });
    });
});