// import {describe, before, it} from "mocha";
// import {step} from "mocha-steps";

const mochaSteps = require('mocha-steps');
const step = mochaSteps.step;


const mocha = require('mocha');
const describe = mocha.describe;
const before = mocha.before;
const it = mocha.it;
const chai = require('chai');
const assert = chai.assert;
const server = require('../server');

/** require the chai-http plugin **/
var chaiHttp = require('chai-http');

/** use the chai-http plugin **/
chai.use(chaiHttp);


describe('Functional Tests2', function () {


    it('requires auth to create a vehicle', function (done) {

        chai.request(server)
            .post('/api/vehicles')
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({
                make: "Peugeot",
                model: "406 Coupe",
                year: "2002",
                countryA: "United States",
                cityA: "Seattle",
                countryB: "Lithuania",
                cityB: "Vilnius",
                bodyStyle: "coupe",
                agreedTermsAndConditions: true
            })
            .end(function (err, res) {
                assert.equal(res.status, 401, 'response status should be 401 (unauthorized)');
                done();
            });
    });


    it('POST bidForVehicle', function (done) {
        chai.request(server)
            .post('/api/vehicles/5d58006b793642ce679d01f7/bid')
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({amount: 300})
            .end(function (err, res) {        // Send the request. Pass a callback in
                // node style. `res` is the response object
                assert.equal(res.status, 401, 'response status should be 401');
                // assert.equal(res.type, 'application/json');
                // assert.property(res.body, 'make', 'Vehicle object has make property');
                // assert.property(res.body, 'model', 'Vehicle object has model property');
                // assert.property(res.body, 'year', 'Vehicle object has year property');
                // assert.property(res.body, 'location', 'Vehicle object has location property');
                done();
            });
    });
});
