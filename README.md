# veh-import-bid

### Covered topics
* Node Package Manager (NPM)
* (Covered) Asynchronous JavaScript
* (Covered) CRUD Operations
* Data Validation
* (Covered) Authentication
* Authorization
* Handling and Logging Errors
* Unit and Integration Testing
* TDD
* Deployment
* Running in containers
* Clean Coding and Refactoring
* Security Best Practices
* (Covered) Many-to-many relationship

## Library selection
### ORMs
* mongoose - more suitable for MongoDB
* sequelize

## Based on tutorials
* [Starting with Authentication (A tutorial with Node.js and MongoDB)](https://medium.com/createdd-notes/starting-with-authentication-a-tutorial-with-node-js-and-mongodb-25d524ca0359)


**Requirements**

* docker
* docker-compose



### Back-end


build container:
```bash
docker-compose up --build backend
```

run:
```bash
docker-compose up  backend
```


### Database

Database container automatically starts on backend startup.

build container:
```bash
docker-compose up --build database
```

run:
```bash
docker-compose up  database