'use strict';

const user = require('./api-user');
const vehicle = require('./api-vehicle');

module.exports = (server) => {
    user(server);
    vehicle(server);
};