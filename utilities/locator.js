'use strict';

var fs = require('fs');
const papa = require('papaparse');
var path = require('path');


module.exports = {

    getVehicleCoordinates(vehicle) {

        return new Promise((resolve, reject) => {
            let p = path.join(path.join(__dirname, "../public"), '/worldcities.csv');

            let contents = fs.readFileSync(p, 'utf8');
            const csvData = papa.parse(contents, {header: true}).data;

            let cityA = csvData.filter(data => data.city_ascii === vehicle.cityA && data.country === vehicle.countryA)[0];

            if (cityA === undefined) reject({
                errorCode: "ORIGIN_COUNTRY_OR_CITY_NOT_FOUND",
                field: "countryA, cityA",
                originalValue: vehicle.cityA,
                message: "Origin city or country not found",
                helpUrl: "no-url"
            });

            let cityB = csvData.filter(data => data.city_ascii === vehicle.cityB && data.country === vehicle.countryB)[0];

            if (cityB === undefined) reject({
                errorCode: "DESTINATION_COUNTRY_OR_CITY_NOT_FOUND",
                field: "countryB, cityB",
                originalValue: vehicle.cityB,
                message: "Destination city or country not found",
                helpUrl: "no-url"
            });

            resolve({
                coordinatesA: {
                    lat: cityA.lat,
                    lng: cityA.lng
                },
                coordinatesB: {
                    lat: cityB.lat,
                    lng: cityB.lng
                }
            });
        });


    }

};