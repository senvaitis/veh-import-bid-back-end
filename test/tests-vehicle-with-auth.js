// import {describe, before, it} from "mocha";
// import {step} from "mocha-steps";

const mochaSteps = require('mocha-steps');
const step = mochaSteps.step;


const mocha = require('mocha');
const describe = mocha.describe;
const before = mocha.before;
const it = mocha.it;
const chai = require('chai');
const assert = chai.assert;
const server = require('../server');
const path = require('path');

/** require the chai-http plugin **/
var chaiHttp = require('chai-http');

/** use the chai-http plugin **/
chai.use(chaiHttp);


const JWS_REGEX = /^[a-zA-Z0-9\-_]+?\.[a-zA-Z0-9\-_]+?\.([a-zA-Z0-9\-_]+)?$/;


describe('Functional Tests2', function () {

    let token;

    before("register", function (done) {
        chai.request(server)
            .post('/api/users/register')
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({
                username: 'autotester1',
                email: 'autotester1@gmail.com',
                password: 'testpass',
                passwordConf: 'testpass'
            })
            .end(function (err, res) {        // Send the request. Pass a callback in
                assert.equal(res.status, 200);
                assert.equal(res.type, 'application/json');
                assert.match(res.body.token, JWS_REGEX);
                token = res.body.token;
                done();
            });
    });


    it('create a new vehicle', function (done) {
        chai.request(server)
            .post('/api/vehicles')
            .set('content-type', 'application/x-www-form-urlencoded')
            .set('Authorization', "JWT " + token)
            .send({
                make: "Peugeot",
                model: "406 Coupe",
                year: "2002",
                countryA: "United States",
                cityA: "Seattle",
                countryB: "Lithuania",
                cityB: "Vilnius",
                bodyStyle: "coupe",
                agreedTermsAndConditions: true
            })
            .end(function (err, res) {
                assert.equal(res.status, 200);
                assert.equal(res.type, 'application/json');
                assert.equal(res.body.make, 'Peugeot');
                assert.equal(res.body.model, '406 Coupe');
                assert.equal(res.body.year, '2002');
                assert.equal(res.body.countryA, 'United States');
                assert.equal(res.body.cityA, 'Seattle');
                assert.equal(res.body.countryB, 'Lithuania');
                assert.equal(res.body.cityB, 'Vilnius');
                assert.equal(res.body.bodyStyle, 'coupe');
                assert.equal(res.body.agreedTermsAndConditions, true);
                done();
            });
    });


    it('Test GET /vehicles', function (done) {
        chai.request(server)
            .get('/api/vehicles')
            .end(function (err, res) {
                assert.equal(res.status, 200);
                assert.equal(res.type, 'application/json');
                res.body.vehicles.forEach(veh => {
                    assert.property(veh, 'make');
                    assert.property(veh, 'model');
                    assert.property(veh, 'year');
                    assert.property(veh, 'countryA');
                    assert.property(veh, 'cityA');
                    assert.property(veh, 'countryB');
                    assert.property(veh, 'cityB');
                    assert.property(veh, 'bids');
                    assert.property(veh, 'currentBid');
                });
                done();
            });
    });


    it('Test GET /vehicles/:id', function (done) {
        chai.request(server)
            .get('/api/vehicles/5d57f31b338eda7fa558b2fa')
            .set('Authorization', "JWT " + token)
            .end(function (err, res) {
                assert.equal(res.status, 200);
                assert.equal(res.type, 'application/json');
                assert.property(res.body, 'make');
                assert.property(res.body, 'model');
                assert.property(res.body, 'year');
                assert.property(res.body, 'countryA');
                assert.property(res.body, 'cityA');
                assert.property(res.body, 'countryB');
                assert.property(res.body, 'cityB');
                done();
            });
    });


    it('POST bidForVehicle amount is 0', function (done) {
        chai.request(server)
            .post('/api/vehicles/5d57f31b338eda7fa558b2fa/bid')
            .set('content-type', 'application/x-www-form-urlencoded')
            .set('Authorization', "JWT " + token)
            .send({amount: 0})
            .end(function (err, res) {
                assert.equal(res.status, 400, 'response status should be 400 (bad request)');
                assert.equal(res.type, 'application/json');
                assert.deepEqual(res.body,
                    {
                        errorCode: "WRONG_BID",
                        field: "amount",
                        originalValue: "0",
                        message: "Bid amount should be lower than the last bid and higher than 0",
                        helpUrl: "no-url"
                    });
                done();
            });
    });

    it('POST bidForVehicle', function (done) {
        chai.request(server)
            .post('/api/vehicles/5d58006b793642ce679d01f7/bid')
            .set('content-type', 'application/x-www-form-urlencoded')
            .set('Authorization', "JWT " + token)
            .send({amount: 300})
            .end(function (err, res) {
                assert.equal(res.status, 200);
                assert.equal(res.type, 'application/json');
                assert.property(res.body, 'make');
                assert.property(res.body, 'model');
                assert.property(res.body, 'year');
                assert.property(res.body, 'countryA');
                assert.property(res.body, 'cityA');
                assert.property(res.body, 'countryB');
                assert.property(res.body, 'cityB');
                done();
            });
    });


    it('POST bidForVehicle amount is equal to previous bid', function (done) {
        chai.request(server)
            .post('/api/vehicles/5d53eaa3f2a2bf85c8ba18cf/bid')
            .set('content-type', 'application/x-www-form-urlencoded')
            .set('Authorization', "JWT " + token)
            .send({amount: 800})
            .end(function (err, res) {
                assert.equal(res.status, 400, 'response status should be 400 (bad request)');
                assert.equal(res.type, 'application/json');
                assert.deepEqual(res.body,
                    {
                        errorCode: "WRONG_BID",
                        field: "amount",
                        originalValue: "800",
                        message: "Bid amount should be lower than the last bid and higher than 0",
                        helpUrl: "no-url"
                    });
                done();
            });
    });

    it('POST bidForVehicle amount is negative', function (done) {
        chai.request(server)
        .post('/api/vehicles/5d53eaa3f2a2bf85c8ba18cf/bid')
        .set('content-type', 'application/x-www-form-urlencoded')
        .set('Authorization', "JWT " + token)
        .send({amount: -100})
        .end(function (err, res) {
            assert.equal(res.status, 400, 'response status should be 400 (bad request)');
            assert.equal(res.type, 'application/json');
            assert.deepEqual(res.body,
                {
                    errorCode: "WRONG_BID",
                    field: "amount",
                    originalValue: "-100",
                    message: "Bid amount should be lower than the last bid and higher than 0",
                    helpUrl: "no-url"
                });
            done();
        });
    });
});
