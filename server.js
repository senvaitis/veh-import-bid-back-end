'use strict';
const dotenv = require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const routes = require('./routes');
const {ExtractJwt, Strategy} = require('passport-jwt');
const User = require("./models").User;
const session = require('express-session');
const cors = require('cors');
const helmet = require('helmet');
const passport = require('passport');
const enableWs = require('express-ws');


// Mongo stores all data associated within one record, instead of storing it across many preset tables as in a SQL database.
var MongoStore = require('connect-mongo')(session);


let server = express();
enableWs(server);



// connect to MongoDB
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
let db = mongoose.connection;

// While an HTML server serves HTML, an API serves data
// An HTML server usually has one or more directories that are accessible by the user. You can place there the static assets needed by your application (stylesheets, scripts, images). In Express you can put in place this functionality using the middleware app.use(path, middlewareFunction)
server.use('/uploads', express.static('uploads'));

server.use(bodyParser.urlencoded({'extended': 'true'}));            // parse application/x-www-form-urlencoded
server.use(bodyParser.json());                                     // parse application/json
server.use(bodyParser.json({type: 'application/vnd.api+json'})); // parse application/vnd.api+json as json


// Helmet default options: https://github.com/helmetjs/helmet
server.use(helmet({}));

server.use(cors({
    origin: true,
    credentials: true
}));


const passportOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
    secretOrKey: process.env.JWT_TOKEN_SECRET
};


const jwtStrategy = new Strategy(passportOptions, (jwt_payload, done) => {
    const username = jwt_payload.username;
    // here you would normally fetch the user from your database

    User.findOne({username: username}, {password: 0}).then((authUser) => {
        if (authUser.username !== username) {
            done(new Error('User not found'), null)
        } else {
            done(null, authUser)
        }
    });
});

passport.use(jwtStrategy);
server.use(passport.initialize());

// Express evaluates functions in the order they appear in the code. This is true for middleware too. If you want it to work for all the routes, it should be mounted before them.
routes(server);


const port = process.env.PORT;
server.listen(port, function () {
    console.log(`Express server is listening on ${port}`);
});


module.exports = server;