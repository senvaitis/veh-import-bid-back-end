'use strict';

const Vehicle = require('./vehicle');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');


const UserSchema = new mongoose.Schema({ // TODO why capital letter
    email: {
        type: String,
        // unique: true,
        required: true,
        trim: true
    },
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
    },
    vehicles: [{
        amount: Number,
        time: { type : Date }
    }]
}, {collection: 'users'});



//hashing a password before saving it to the database
// UserSchema.pre('save', function (next) {
//     var user = this;
//     // As hashing is designed to be computationally intensive, it is recommended to do so asyncronously on your server as to avoid blocking incoming connections while you hash
//     bcrypt.hash(user.password, 10, function (err, hash) {
//         if (err) {
//             return next(err);
//         }
//         user.password = hash;
//         next();
//     })
// });


var User = mongoose.model('User', UserSchema);

module.exports = User;